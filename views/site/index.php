<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Seleccion 3</h1>

        <p class="lead">Modulo 3 - Unidad 2</p>
    </div>

 <div class="body-content">
        <div class="row">
            <!-- 
            boton de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 3</h3>
                        <p>Listar el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta3a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta3'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
            <!-- 
            boton de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 4</h3>
                        <p>Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta4a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta4'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
            <!-- 
            boton de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 8</h3>
                        <p>Listar el nombre de los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta8a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta8'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
            <!-- 
            boton de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 12</h3>
                        <p>Listar el numero de ciclistas que hayan ganado algun puerto</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta12a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta12'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
            <!-- 
            boton de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 14</h3>
                        <p>Indicar la altura media de los puertos</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta14a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta14'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
            <!-- 
            boton de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 18</h3>
                        <p>Listar el dorsal del ciclista con el codigo de maillot y cuantas veces ese ciclista ha llevado ese maillot</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta18a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta18'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
        </div>
    </div>
</div>
