<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCrud() {
        return $this->render("gestion");
    }
      
    public function actionConsulta3a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("dorsal")
                ->distinct()
                ->where("nomequipo='Banesto'")
                ->andWhere("edad BETWEEN 25 AND 32"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 3 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32",
        ]);
    }
    
    public function actionConsulta3() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('SELECT count(dorsal) FROM ciclista WHERE nomequipo="Banesto" AND edad BETWEEN 25 AND 32')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM ciclista WHERE nomequipo="Banesto" AND edad BETWEEN 25 AND 32',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 3 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32",
        ]);
    }
    
    public function actionConsulta4a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("dorsal")
                ->distinct()
                ->where("nomequipo='Banesto'")
                ->orWhere("edad BETWEEN 25 AND 32"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 4 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32",
        ]);
    }
    
    public function actionConsulta4() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('SELECT count(dorsal) FROM ciclista WHERE nomequipo="Banesto" OR edad BETWEEN 25 AND 32')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM ciclista WHERE nomequipo="Banesto" OR edad BETWEEN 25 AND 32',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 4 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32",
        ]);
    }
    
    public function actionConsulta8a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("nompuerto")
                ->distinct()
                ->where("altura BETWEEN 1000 AND 2000")
                ->orWhere("altura>2400"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=> "Consulta 8 con Active Record",
            "enunciado"=>"Listar el nombre de los puestos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400",
        ]);
    }
    
    public function actionConsulta8() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('SELECT count(nompuerto) FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=> "Consulta 8 con DAO",
            "enunciado"=>"Listar el nombre de los puestos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400",
        ]);
    }
    
    public function actionConsulta12a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("count(distinct dorsal) total")
                ->distinct()
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=> "Consulta 12 con Active Record",
            "enunciado"=>"Listar el numero de ciclistas que hayan ganado algun puerto",
            "sql"=>"SELECT COUNT(DISTINCT dorsal) FROM puerto",
        ]);
    }
    
    public function actionConsulta12() {
        // mediante DAO
        $numero=1;
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT COUNT(DISTINCT dorsal) numero FROM puerto',
            'totalCount'=>$numero,
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=> "Consulta 12 con DAO",
            "enunciado"=>"Listar el numero de ciclistas que hayan ganado algun puerto",
            "sql"=>"SELECT COUNT(DISTINCT dorsal) FROM puerto",
        ]);
    }
    
    public function actionConsulta14a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("avg(altura) total")
                ->distinct()
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=> "Consulta 14 con Active Record",
            "enunciado"=>"Indicar la altura media de los puertos",
            "sql"=>"SELECT AVG(altura) FROM puerto",
        ]);
    }
    
    public function actionConsulta14() {
        // mediante DAO
        $numero=1;
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT AVG(altura) numero FROM puerto',
            'totalCount'=>$numero,
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero'],
            "titulo"=> "Consulta 14 con DAO",
            "enunciado"=>"Indicar la altura media de los puertos",
            "sql"=>"SELECT AVG(altura) FROM puerto",
        ]);
    }
    
    public function actionConsulta18a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Lleva::find()
                ->select("dorsal,código,COUNT(*) total")
                ->distinct()
                ->groupBy("dorsal,código"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','código','total'],
            "titulo"=> "Consulta 18 con Active Record",
            "enunciado"=>"Listar el dorsal del ciclista con el codigo de maillot y cuantas veces ese ciclista ha llevado ese maillot",
            "sql"=>"SELECT dorsal,código,COUNT(*) FROM lleva GROUP BY dorsal,código",
        ]);
    }
    
    public function actionConsulta18() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('SELECT COUNT(*) FROM lleva GROUP BY dorsal,código')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT dorsal,código,COUNT(*) numero FROM lleva GROUP BY dorsal,código',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','código','numero'],
            "titulo"=> "Consulta 18 con DAO",
            "enunciado"=>"Listar el dorsal del ciclista con el codigo de maillot y cuantas veces ese ciclista ha llevado ese maillot",
            "sql"=>"SELECT dorsal,código,COUNT(*) FROM lleva GROUP BY dorsal,código",
        ]);
    }
    
    
}